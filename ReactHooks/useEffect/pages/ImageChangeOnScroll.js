import React from 'react';
import ImageToggleOnScroll from '../src/ImageToggleOnScroll';

const ImageChangeOnScroll = () => {
    return (
        <div>
            <ImageToggleOnScroll primaryImg="/static/speakers/bw/cherna.png"
                                    secondaryImg="/static/speakers/cvetna.png"
                                    alt="" />
            <br />
            <ImageToggleOnScroll primaryImg="/static/speakers/bw/cherna2.png"
                                    secondaryImg="/static/speakers/cvetna2.png"
                alt="" />
            <br />
            <ImageToggleOnScroll primaryImg="/static/speakers/bw/cherna.png"
                secondaryImg="/static/speakers/cvetna.png"
                alt="" />
            <br />
            <ImageToggleOnScroll primaryImg="/static/speakers/bw/cherna2.png"
                secondaryImg="/static/speakers/cvetna2.png"
                alt="" />
            <br />
            <ImageToggleOnScroll primaryImg="/static/speakers/bw/cherna.png"
                secondaryImg="/static/speakers/cvetna.png"
                alt="" />
        </div>    
    );
};

export default ImageChangeOnScroll;