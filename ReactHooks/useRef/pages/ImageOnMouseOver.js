import React from 'react';
import ImageToggleOnMouseOver from '../src/ImageToggleOnMouseOver';

const ImageOnMouseOver = () => {
    return (
        <div>
            <ImageToggleOnMouseOver primaryImg="/static/speakers/bw/cherna.png"
                                    secondaryImg="/static/speakers/cvetna.png"
                                    alt="" />
            <ImageToggleOnMouseOver primaryImg="/static/speakers/cvetna.png"
                                    secondaryImg="/static/speakers/bw/cherna.png"
                                    alt="" />
            <br />
            <ImageToggleOnMouseOver primaryImg="/static/speakers/bw/cherna2.png"
                                    secondaryImg="/static/speakers/cvetna2.png"
                                    alt="" />
            <ImageToggleOnMouseOver primaryImg="/static/speakers/cvetna2.png"
                                    secondaryImg="/static/speakers/bw/cherna2.png"
                                    alt="" />
        </div>    
    );
};

export default ImageOnMouseOver;