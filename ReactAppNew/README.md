This react app is created without create-react-app. This way you can install only the things that you need. This is just a basic react app
that builds it's own server and it has it's own testing system. This is like when you run npm eject on create-react-app, but with only the needed
things for react to build properly.

To run this react app you have to run two commands on different consoles in this folder.
1. Go in ReactAppNew and open your command panel then type 'npm run dev:bundle'
2. Go in ReactAppNew and open second command panel and type 'npm run dev:server'


If you don't have your 4242 port free and you can't free it right now, go in ReactAppNew/server/server.js
and change the server.listen(4242, ... to whatever port you can use.