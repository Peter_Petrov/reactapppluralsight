import React from 'react';

function ButtonAdd(props) {
    const handleClick = () => props.onClickFunction(props.increment);
    return (
        <button onClick={handleClick}>+{props.increment}</button>
    );
}

export default ButtonAdd;
