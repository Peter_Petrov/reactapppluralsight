import React, { useState } from 'react';
import ButtonAdd from './ButtonAdd';
import Display from './Display';


function App() {
    const [counter, setCounter] = useState(5);
    const incrementCounter = (incrementValue) => setCounter(counter + incrementValue);

    return (
        <div>
            This is a sample stateful and server-side
            rendered React application.
            <br />
            <br />
            Here is a button that will track
            how many times you click it:
            <br />
            <br />
            <React.Fragment>
                <ButtonAdd onClickFunction={incrementCounter} increment={1} />
                <Display message={counter} />
            </React.Fragment>
        </div>
    );
}

export default App;