module.exports = {
    parser: 'babel-eslint',
    env: {
        browser: true,
        commonjs: true,
        es6: true,
        node: true,
        jest: true,
    },
    
    parserOptions: {
        ecmaVersion: 2020,
        ecmaFeatures: {
            impliedStrict: true,
            jsx: true,
        },
        sourseType: 'module',
    },
    plugins: ['react', 'react-hooks'],
    extends: [
        'eslint:recommended',
        'plugin:react/reacommended',
        'plugin:react-hooks/recommended'
    ],
    settings: {
        react: {
            version: 'detect',
        },
    },
    rules: {
        // We can do your custimazation here for the recommended settings
        // For example if we don't want to use the prop-types package
        // we can turn off that recommended rule with 'react/prop-types: ['off'], like I did it
        'react/prop-types': ['off']
    },
};