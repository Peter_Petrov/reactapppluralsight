import React from 'react';

function ButtonSubstitute(props) {
    const handleClick = () => props.onClickFunction(props.increment);
    return (
        <button onClick={handleClick}>-{props.increment}</button>
    );
}

export default ButtonSubstitute;
