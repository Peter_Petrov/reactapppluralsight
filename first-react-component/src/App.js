import React, { useState } from 'react';
import ButtonAdd from './ButtonAdd';
import ButtonSubstitute from './ButtonSubstitute';
import ButtonMultiply from './ButtonMultiply';
import ButtonDivide from './ButtonDivide';
import Display from './Display';

function App() {
    const [counter, setCounter] = useState(5);
    const incrementCounter = (incrementValue) => setCounter(counter + incrementValue);
    const incrementCounter2 = (incrementValue) => setCounter(counter - incrementValue);
    const incrementCounter3 = (incrementValue) => setCounter(counter * incrementValue);
    const incrementCounter4 = (incrementValue) => setCounter(counter / incrementValue);

    return (
        <React.Fragment>
            <ButtonAdd onClickFunction={incrementCounter} increment={1} />
            <ButtonAdd onClickFunction={incrementCounter} increment={5} />
            <ButtonAdd onClickFunction={incrementCounter} increment={10} />
            <ButtonAdd onClickFunction={incrementCounter} increment={100} />
            <ButtonAdd onClickFunction={incrementCounter} increment={1000} />
            <ButtonSubstitute onClickFunction={incrementCounter2} increment={1} />
            <ButtonSubstitute onClickFunction={incrementCounter2} increment={5} />
            <ButtonSubstitute onClickFunction={incrementCounter2} increment={10} />
            <ButtonSubstitute onClickFunction={incrementCounter2} increment={100} />
            <ButtonSubstitute onClickFunction={incrementCounter2} increment={1000} />
            <ButtonMultiply onClickFunction={incrementCounter3} increment={2} />
            <ButtonMultiply onClickFunction={incrementCounter3} increment={5} />
            <ButtonMultiply onClickFunction={incrementCounter3} increment={10} />
            <ButtonMultiply onClickFunction={incrementCounter3} increment={20} />
            <ButtonMultiply onClickFunction={incrementCounter3} increment={50} />
            <ButtonDivide onClickFunction={incrementCounter4} increment={2} />
            <ButtonDivide onClickFunction={incrementCounter4} increment={5} />
            <ButtonDivide onClickFunction={incrementCounter4} increment={10} />
            <ButtonDivide onClickFunction={incrementCounter4} increment={20} />
            <ButtonDivide onClickFunction={incrementCounter4} increment={50} />
            <Display message={counter}/>
        </React.Fragment>
    );
}

export default App;